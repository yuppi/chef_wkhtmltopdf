#
# Cookbook Name:: wkhtmltopdf
# Recipe:: default
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#--------------------------------------------------------------------
# 依存パッケージのインストール
#--------------------------------------------------------------------
%w{fontconfig freetype libpng libjpeg libX11 libXext libXrender xorg-x11-fonts-Type1 xorg-x11-fonts-75dpi}.each do | pkg |
  yum_package pkg do
    action :install
    options "--enablerepo=epel --enablerepo=remi --enablerepo=remi-php56"
  end
end

#--------------------------------------------------------------------
# 設定情報
#--------------------------------------------------------------------
# wkhtmltopdfのRPMを置くディレクトリを作成
directory node['wkhtmltopdf']['default']['work_dir'] do
  action :create
  recursive true
  not_if "ls -d #{node['wkhtmltopdf']['default']['work_dir']}"
end

# wkhtmltopdfのRPMをDL＠RHEL6
remote_file node['wkhtmltopdf']['default']['work_dir'] + node['wkhtmltopdf']['default']['source_file_name'] do
  source node['wkhtmltopdf']['default']['source_url_path'] + node['wkhtmltopdf']['default']['source_file_name']
  not_if "ls #{node['wkhtmltopdf']['default']['work_dir']}#{node['wkhtmltopdf']['default']['source_file_name']}"
end

#--------------------------------------------------------------------
# wkhtmltopdf公式のリポジトリを追加する
#--------------------------------------------------------------------
# RPMをインストール
rpm_package "wkhtmltopdf-rpm" do
  action :install
  source node['wkhtmltopdf']['default']['work_dir'] + node['wkhtmltopdf']['default']['source_file_name']
end