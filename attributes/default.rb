# 作業用ディレクトリ
default['wkhtmltopdf']['default']['work_dir'] = '/usr/local/src/wkhtmltopdf/'

# ソースコードの URL
default['wkhtmltopdf']['default']['source_version']      = '0.12.2.1'
default['wkhtmltopdf']['default']['source_url_path']     = "http://downloads.sourceforge.net/project/wkhtmltopdf/#{default['wkhtmltopdf']['default']['source_version']}/"
default['wkhtmltopdf']['default']['source_file_name']    = "wkhtmltox-#{default['wkhtmltopdf']['default']['source_version']}_linux-centos6-amd64.rpm"